// ==============================

var express = require('express');
var http = require('http');
var path = require('path');
var bodyParser = require('body-parser');
var livereload = require('livereload');
var RewriteMiddleware = require('express-htaccess-middleware');

var app = express();

var publicDir = path.join(__dirname, 'app');

//process.env.PORT = 4000;
app.set('port', process.env.PORT || 4000);
//app.use(logger('dev'));
app.use(express.static(publicDir));
app.use(bodyParser.json()); //parses json, multi-part (file), url-encoded 

app.use(RewriteMiddleware({
    file: path.resolve(publicDir, '.htaccess'),
    verbose: true
}));

app.get('/', function (req, res) {
    res.sendFile(path.join(publicDir, 'index.html'));
});

var server = http.createServer(app);
server.listen(app.get('port'), function () {
    console.log("Web server listening on port " + app.get('port'));
});

var lrserver = livereload.createServer({
    exclusions: "bower_components/"
});
lrserver.watch(publicDir);